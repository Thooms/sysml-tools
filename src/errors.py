"""
exceptions.py

Gathers the exceptions used by the whole tool.
"""

class AIRError(Exception):
    """Base class for AIR exceptions."""
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class BackendError(AIRError):
    """Exceptions concerning the backend."""
    pass

class DiagramError(AIRError):
    """Exceptions concerning diagram building."""
    pass

class BadXMIException(Exception):
  def __init__(self, what):
    self.what = what
  def __str__(self):
    return 'Bad XMI file\t' + self.what

class FTAError(Exception):
  def __init__(self, what):
    self.what = what
  def __str__(self):
    return self.what

class FTADrawingError(Exception):
    def __init__(self, what):
        self.what = what
    def __str__(self):
        return self.what

class FTAPSAError(Exception):
    def __init__(self, what):
        self.what = what
    def __str__(self):
        return self.what

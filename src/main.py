"""sysml-tools.

Usage:
  main.py <input_xmi> <output_xls>

Options:
  -h --help Automatically generated
"""

import parser.activity
import parser.internal_block
import fmea.functional
import fmea.component
from fta.generate import FTA
from fta.fta_drawing import *
from fta import open_psa
import nusmv.generate
from errors import *

import gui

import Tkinter
import tkFileDialog
import tkMessageBox


##### OPEN FUNCTIONAL FMEA #####

def open_functional_fmea_command(listbox, contents):
  """
    Command executed as soon as the 'Open existing component FMEA' has been pressed

    TODO: Handle retroaction (XMI -> FMEA -> XMI)
  """
  pass

open_functional_fmea = gui.GUIButton(
  text='Open existing functional FMEA',
  row=2,
  column=0,
  command=open_functional_fmea_command,
  needs_xmi=False) # Doesn't need an XMI tree to work

################################


##### GENERATE FUNCTIONAL FMEA #####

def generate_functional_fmea_command(root, listbox, contents):
  """
    Command executed as soon as the "Generate Functional FMEA" button has been pressed.

      root: The root of the XMI tree coming from the opened XMI file
  """

  content = contents[listbox.index(Tkinter.ACTIVE)]

  if content is None or 'Functional FMEA' not in content.applications:
    tkMessageBox.showerror('Error!', 'No Functional FMEA available for this package.')
    return

  # File explorer, lists only directories and '.xls' files, bacause the output will be a .xls file
  fl = tkFileDialog.asksaveasfile(mode='w', defaultextension='.xls')
  if fl:
    xls_file = fl.name
    fl.close()

    try:

      diagram = parser.activity.diagram(content.root)
      fmea.functional.from_diagram(diagram, xls_file)
      tkMessageBox.showinfo('Success!', 'Functional FMEA successfully created.')

    except DiagramError as e:
      tkMessageBox.showerror('Diagram error!', str(e))

    except BadXMIException as e:
      tkMessageBox.showerror('Bad XMI!', str(e))
  else:
    tkMessageBox.showerror('Error!', 'Functional FMEA not created.')

generate_functional_fmea = gui.GUIButton(
  text='Generate functional FMEA',
  row=2,
  column=0,
  command=generate_functional_fmea_command,
  needs_xmi=True) # Needs an XMI tree to work

####################################


##### OPEN COMPONENT FMEA #####

def open_component_fmea_command(listbox, contents):
  """
    Command executed as soon as the 'Open existing component FMEA' has been pressed

    TODO: Handle retroaction (XMI -> FMEA -> XMI)
  """
  pass

open_component_fmea = gui.GUIButton(
  text='Open existing component FMEA',
  row=3,
  column=0,
  command=open_component_fmea_command,
  needs_xmi=False) # Doesn't need an XMI tree to work

###############################


##### GENERATE COMPONENT FMEA #####

def generate_component_fmea_command(root, listbox, contents):
  """
    Command executed as soon as the "Generate Component FMEA" button has been pressed.

      root: The root of the XMI tree coming from the opened XMI file

    TODO: Handle component FMEA
  """
  
  content = contents[listbox.index(Tkinter.ACTIVE)]

  if content is None or 'Component FMEA' not in content.applications:
    tkMessageBox.showerror('Error!', 'No Component FMEA available for this package.')
    return

  # File explorer, lists only directories and '.xls' files, bacause the output will be a .xls file
  fl = tkFileDialog.asksaveasfile(mode='w', defaultextension='.xls')
  if fl:
    xls_file = fl.name
    fl.close()

    try:

      diagram = parser.activity.diagram(root)
      fmea.component.from_diagram(diagram, xls_file)
      tkMessageBox.showinfo('Success!', 'Component FMEA successfully created.')

    except DiagramError as e:
      tkMessageBox.showerror('Diagram error!', str(e))

    except BadXMIException as e:
      tkMessageBox.showerror('Bad XMI!', str(e))
  else:
    tkMessageBox.showerror('Error!', 'Component FMEA not created.')


generate_component_fmea = gui.GUIButton(
  text='Generate component FMEA',
  row=3,
  column=0,
  command=generate_component_fmea_command,
  needs_xmi=True) # Needs an XMI tree to work

###################################


##### GENERATE SVG FTA #####

def generate_fta_command(root, listbox, contents):
  """
    Command executed as soon as the "Generate SVG FTA" button has been pressed

      root: The root of the XMI tree coming from the opened XMI file

    TODO: Output fault trees as '.svg' files
  """

  content = contents[listbox.index(Tkinter.ACTIVE)]

  if content is None or 'FTA' not in content.applications:
    tkMessageBox.showerror('Error!', 'No FTA available for this package.')
    return

  # File explorer, list directories
  directory = tkFileDialog.askdirectory(title='FTA output directory')
  if directory:

    try:
      # XMI root -> Internal Block Diagram representation
      diagram = parser.internal_block.diagram(root)
      ports = parser.internal_block.ports(content.root)

      # Internal Block Diagram representation -> FTA (possible multiple trees)
      fta = FTA(diagram, ports)

      print fta.trees

      s = ''
      for tree in fta.trees:
        lay = TreeLayout(tree)
        layout1(lay)
        drawTree(lay, directory + '/' + tree.name + '.svg')
        s += tree.name + '.svg; '

      tkMessageBox.showinfo('Success!', 'Fault Tree(s) ' + s + 'successfully created.')
        
    except DiagramError as e:
      tkMessageBox.showerror('Diagram error!', str(e))

    except FTAError as e:
      tkMessageBox.showerror('FTA error!', str(e))
  else:
    tkMessageBox.showerror('Error!', 'Fault tree not created.')

generate_fta = gui.GUIButton(
  text='Generate SVG FTA',
  row=4,
  column=0,
  command=generate_fta_command,
  needs_xmi=True) # Needs an XMI tree to work

########################


##### GENERATE OPEN PSA FTA #####

def generate_psa_fta_command(root, listbox, contents):
  """
    Command executed as soon as the "Generate OPEN PSA FTA" button has been pressed

      root: The root of the XMI tree coming from the opened XMI file
  """

  content = contents[listbox.index(Tkinter.ACTIVE)]

  if content is None or 'FTA' not in content.applications:
    tkMessageBox.showerror('Error!', 'No FTA available for this package.')
    return

  # File explorer, list directories
  directory = tkFileDialog.askdirectory(title='FTA output directory')
  if directory:
    try:
      # XMI root -> Internal Block Diagram representation
      diagram = parser.internal_block.diagram(root)
      ports = parser.internal_block.ports(content.root)

      # Internal Block Diagram representation -> FTA (possible multiple trees)
      fta = FTA(diagram, ports)

      print fta.trees

      s = ''
      for tree in fta.trees:
        with open(directory + '/' + tree.name + '.psa', 'w') as f:
          f.write(open_psa.tree_to_psa(tree))
          s += tree.name + '.psa; '

      tkMessageBox.showinfo('Success!', 'Fault Tree(s) ' + s + ' successfully created.')

    except DiagramError as e:
      tkMessageBox.showerror('Diagram error!', str(e))

    except FTAError as e:
      tkMessageBox.showerror('FTA error!', str(e))
  else:
    tkMessageBox.showerror('Error!', 'Fault tree not created.')

generate_psa_fta = gui.GUIButton(
  text='Generate OPEN PSA FTA',
  row=4,
  column=1,
  command=generate_psa_fta_command,
  needs_xmi=True) # Needs an XMI tree to work

#################################


##### GENERATE NuSMV #####

def generate_nusmv_command(root, listbox, contents):
  """
    Command executed as soon as the "Generate NuSMV" button has been pressed

      root: The root of the XMI tree coming from the opened XMI file
  """

  # File explorer, lists only directories and '.smv' files, bacause the output will be a .smv file
  fl = tkFileDialog.asksaveasfile(mode='w', defaultextension='.smv')
  if fl:
    smv_file = fl.name
    fl.close()

    try:
      smv = nusmv.generate.from_tree(root)

      with open(smv_file, 'w') as f:
        f.write('\n\n'.join([o.generate() for o in smv]))

      tkMessageBox.showinfo('Success!', 'NuSMV successfully created.')

    except DiagramError as e:
      tkMessageBox.showerror('Diagram error!', str(e))

    except FTAError as e:
      tkMessageBox.showerror('FTA error!', str(e))
  else:
    tkMessageBox.showerror('Error!', 'Fault tree not created.')

generate_nusmv = gui.GUIButton(
  text='Generate NuSMV',
  row=5,
  column=0,
  command=generate_nusmv_command,
  needs_xmi=True) # Needs an XMI tree to work

#################################


if __name__ == '__main__':

  master = Tkinter.Tk()

  main_menu = gui.SysMLMenu(
    master=master,

    # Buttons shown in the GUI as soon as a XMI file has been opened
    # (see gui.py doc for more infos)
    
    on_input_buttons=[
      # open_functional_fmea,
      generate_functional_fmea,
      # open_component_fmea,
      generate_component_fmea,
      generate_fta,
      generate_psa_fta,
      generate_nusmv
    ])

  master.mainloop()

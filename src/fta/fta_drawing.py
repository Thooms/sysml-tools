# coding: utf-8

import svgwrite
from svgwrite import cm, mm
from math import cos, sin, pi

from generate import FTA, LogicalGate, Leaf, FTANode
from errors import FTADrawingError


CIRCLERADIUS = 30
RATIO = 2
XPAD = 1
YPAD = 1
UNIT = 50
TSIZE = 7
CHARS_PER_LINE = 10

#
# Tree layout generation
#

class TreeLayout:
    """ Used to store the graphical layout of the tree. We will
    optimize everything here, and eventually produce a SVG drawing.
    
    We will use almost the same techniques as described here :
    http://billmill.org/pymag-trees/

    TODO: offsets, threads, contours, n-ary tree fapping

    """

    def __init__(self, tree, parent=None, depth=0, number=1):
        #if not isinstance(self, FTA):
         #   raise FTADrawingError('Expected fault tree to generate tree layout.')
        #print tree.__class__.__name__
        self.x = -1
        self.y = depth
        self.tree = tree

        if isinstance(tree, FTANode):
            self.text = repr(tree)
            self.type = 'FTANode'
        elif isinstance(tree, LogicalGate):
            self.text = tree.type
            self.type = tree.type + 'LogicalGate'
        elif isinstance(tree, Leaf):
            self.text = repr(tree)
            self.type = 'Leaf'

        self.children = []

        i = 0
        for c in tree.children:
            if isinstance(c, LogicalGate) or repr(c) not in [x.text for x in self.children]:
                self.children.append(TreeLayout(c, self, depth+1, i+1))
                i += 1

        self.parent = parent
        self.thread = None
        self.mod = 0
        self.ancestor = self
        self.change = self.shift = 0
        self._lmost_sibling = None
        #this is the number of the node in its group of siblings 1..n
        self.number = number
        
    def left(self): 
        return self.thread or len(self.children) and self.children[0]

    def right(self):
        return self.thread or len(self.children) and self.children[-1]

    def lbrother(self):
        n = None
        if self.parent:
            for node in self.parent.children:
                if node == self: return n
                else:            n = node
        return n

    def get_lmost_sibling(self):
        if not self._lmost_sibling and self.parent and self != \
        self.parent.children[0]:
            self._lmost_sibling = self.parent.children[0]
        return self._lmost_sibling
    
    lmost_sibling = property(get_lmost_sibling)

    
    
    def __repr__(self):
        return '[' + str(self.x) + ', ' + str(self.y) + ']' + ' {' + ', '.join(map(lambda l: repr(l) if l else '', self.children)) + ' }'



nexts = [0] * 500

def layout1(tree, depth=0):
    tree.x = XPAD + nexts[depth] * RATIO
    tree.y = YPAD + depth * RATIO
    nexts[depth] += 1
    for c in tree.children:
        layout1(c, depth+1)



def layout2(tree):
    dt = firstwalk(TreeLayout(tree))
    min = second_walk(dt)
    if min < 0:
        third_walk(dt, -min)
    return dt

def third_walk(tree, n):
    tree.x += n
    for c in tree.children:
        third_walk(c, n)

def firstwalk(v, distance=1.):
    if len(v.children) == 0:
        if v.lmost_sibling:
            v.x = v.lbrother().x + distance
        else:
            v.x = 0.
    else:
        default_ancestor = v.children[0]
        for w in v.children:
            firstwalk(w)
            default_ancestor = apportion(w, default_ancestor, distance)
        print "finished v =", v.tree, "children"
        execute_shifts(v)

        midpoint = (v.children[0].x + v.children[-1].x) / 2

        ell = v.children[0]
        arr = v.children[-1]
        w = v.lbrother()
        if w:
            v.x = w.x + distance
            v.mod = v.x - midpoint
        else:
            v.x = midpoint
    return v

def apportion(v, default_ancestor, distance):
    w = v.lbrother()
    if w is not None:
        #in buchheim notation:
        #i == inner; o == outer; r == right; l == left; r = +; l = -
        vir = vor = v
        vil = w
        vol = v.lmost_sibling
        sir = sor = v.mod
        sil = vil.mod
        sol = vol.mod
        while vil.right() and vir.left():
            vil = vil.right()
            vir = vir.left()
            vol = vol.left()
            vor = vor.right()
            vor.ancestor = v
            shift = (vil.x + sil) - (vir.x + sir) + distance
            if shift > 0:
                move_subtree(ancestor(vil, v, default_ancestor), v, shift)
                sir = sir + shift
                sor = sor + shift
            sil += vil.mod
            sir += vir.mod
            sol += vol.mod
            sor += vor.mod
        if vil.right() and not vor.right():
            vor.thread = vil.right()
            vor.mod += sil - sor
        else:
            if vir.left() and not vol.left():
                vol.thread = vir.left()
                vol.mod += sir - sol
            default_ancestor = v
    return default_ancestor

def move_subtree(wl, wr, shift):
    subtrees = wr.number - wl.number
    print wl.tree, "is conflicted with", wr.tree, 'moving', subtrees, 'shift', shift
    #print wl, wr, wr.number, wl.number, shift, subtrees, shift/subtrees
    wr.change -= shift / subtrees
    wr.shift += shift
    wl.change += shift / subtrees
    wr.x += shift
    wr.mod += shift

def execute_shifts(v):
    shift = change = 0
    for w in v.children[::-1]:
        print "shift:", w, shift, w.change
        w.x += shift
        w.mod += shift
        change += w.change
        shift += w.shift + change

def ancestor(vil, v, default_ancestor):
    #the relevant text is at the bottom of page 7 of
    #"Improving Walker's Algorithm to Run in Linear Time" by Buchheim et al, (2002)
    #http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.16.8757&rep=rep1&type=pdf
    if vil.ancestor in v.parent.children:
        return vil.ancestor
    else:
        return default_ancestor

def second_walk(v, m=0, depth=0, min=None):
    v.x += m
    v.y = depth

    if min is None or v.x < min:
        min = v.x

    for w in v.children:
        min = second_walk(w, m + v.mod, depth+1, min)

    return min
    


#
# SVG output primitives
#                      
# TODO: classes for primitive blocks, draw_svg with nodes, add
#   edges (straight, beziers, diagonal), export as png/jpeg.                      

def addText(dwg, x, y, text):
    texts = [text[i:i+CHARS_PER_LINE] for i in range(0, len(text), CHARS_PER_LINE)]

    for i, t in enumerate(texts):
        dwg.add(dwg.text(t, insert=(x, y + (i - len(texts) / 2) * (TSIZE + 3)), style='font-size:{}pt;'.format(TSIZE)))

def addSVGNode(dwg, x, y, text):
    """ Adds a node centered at (x, y). """
    dwg.add(dwg.circle(center=(x * UNIT, y * UNIT), r=CIRCLERADIUS, stroke='black', fill='white'))
    addText(dwg, x * UNIT - CIRCLERADIUS / 2 - CHARS_PER_LINE/2, y * UNIT + CIRCLERADIUS / 4, text)
    # dwg.add(dwg.text(text + "\nfdgjh", style="font-size:{}pt;".format(TSIZE), insert=(x * UNIT - CIRCLERADIUS / 2 - len(text)/2, y * UNIT + CIRCLERADIUS / 4)))

def addOrSVGNode(dwg, x, y, text):
    """ Adds an OR node at (x, y), represented as a 6-line polygon. """
    points = []
    points.append((x * UNIT, y * UNIT - CIRCLERADIUS))
    points.append((x * UNIT + (CIRCLERADIUS * cos(pi / 4.)), y * UNIT - (CIRCLERADIUS * sin(pi / 4.))))
    points.append((x * UNIT + (CIRCLERADIUS * cos(-pi / 4.)), y * UNIT - 2. * CIRCLERADIUS * sin(-pi / 4.)))
    points.append((x * UNIT, y * UNIT + CIRCLERADIUS))
    points.append((x * UNIT + (CIRCLERADIUS * cos(-3. * pi / 4.)), y * UNIT - 2. * CIRCLERADIUS * sin(-3. * pi / 4.)))
    points.append((x * UNIT + (CIRCLERADIUS * cos(3. * pi / 4.)), y * UNIT - (CIRCLERADIUS * sin(3. * pi / 4.))))

    dwg.add(dwg.polygon(points=points, stroke='black', fill='white'))
    addText(dwg, x * UNIT - CIRCLERADIUS / 4, y * UNIT + CIRCLERADIUS / 4, text)
    # dwg.add(dwg.text(text, style="font-size:{}pt;".format(TSIZE), insert=(x * UNIT - CIRCLERADIUS / 2 - len(text)/2, y * UNIT + CIRCLERADIUS / 4)))

def addAndSVGNode(dwg, x, y, text):
    """ Adds an OR node at (x, y), represented as a 6-line polygon. """
    points = []
    points.append((x * UNIT, y * UNIT - CIRCLERADIUS))
    points.append((x * UNIT + (CIRCLERADIUS * cos(pi / 4.)), y * UNIT - (CIRCLERADIUS * sin(pi / 4.))))
    points.append((x * UNIT + (CIRCLERADIUS * cos(-pi / 4.)), y * UNIT + CIRCLERADIUS))
    points.append((x * UNIT + (CIRCLERADIUS * cos(-3. * pi / 4.)), y * UNIT + CIRCLERADIUS))
    points.append((x * UNIT + (CIRCLERADIUS * cos(3. * pi / 4.)), y * UNIT - (CIRCLERADIUS * sin(3. * pi / 4.))))

    dwg.add(dwg.polygon(points=points, stroke='black', fill='white'))
    addText(dwg, x * UNIT - CIRCLERADIUS / 4, y * UNIT + CIRCLERADIUS / 4, text)
    # dwg.add(dwg.text(text, style="font-size:{}pt;".format(TSIZE), insert=(x * UNIT - CIRCLERADIUS / 2 - len(text)/2, y * UNIT + CIRCLERADIUS / 4)))

def addFTASVGNode(dwg, x, y, text):
    dwg.add(dwg.rect(insert=(x * UNIT - CIRCLERADIUS, y * UNIT - CIRCLERADIUS), size=(2 * CIRCLERADIUS, 2 * CIRCLERADIUS), stroke='black', fill='white'))
    addText(dwg, x * UNIT - CIRCLERADIUS / 2 - CHARS_PER_LINE/2, y * UNIT + CIRCLERADIUS / 4, text)
    # dwg.add(dwg.text(text, style="font-size:{}pt;".format(TSIZE), insert=(x * UNIT - CIRCLERADIUS / 2 - CHARS_PER_LINE/2, y * UNIT + CIRCLERADIUS / 4)))

def drawNodes(tree_layout, dwg):
    """ High level function to draw nodes. """

    if tree_layout.type == 'FTANode':
        addFTASVGNode(dwg, tree_layout.x, tree_layout.y, tree_layout.text)
    elif tree_layout.type == 'ORLogicalGate':
        addOrSVGNode(dwg, tree_layout.x, tree_layout.y, tree_layout.text)
    elif tree_layout.type == 'ANDLogicalGate':
        addAndSVGNode(dwg, tree_layout.x, tree_layout.y, tree_layout.text)
    else:
        addSVGNode(dwg, tree_layout.x, tree_layout.y, tree_layout.text)

    for c in tree_layout.children:
        drawNodes(c, dwg)   

def connectSVGNodes(dwg, x1, y1, x2, y2):
    """ Connects two nodes. We assume that the upper circle
    has coordinates (x1, x2).
    """
    interpoints = [(x1 * UNIT, y1 * UNIT + CIRCLERADIUS), \
                       (x1 * UNIT, ((y1 + y2) / 2) * UNIT), \
                       (x2 * UNIT, ((y1 + y2) / 2) * UNIT), \
                       (x2 * UNIT, y2 * UNIT - CIRCLERADIUS)]
    
    edges = dwg.add(dwg.g(id='edge', stroke='black'))

    for i in xrange(len(interpoints) - 1):
        #print str(interpoints[i]) + ' -> ' + str(interpoints[i+1])
        edges.add(dwg.line(start=interpoints[i], end=interpoints[i+1]))

def drawEdges(tree_layout, dwg):
    """ High level function to draw edges. """
    for c in tree_layout.children:
        connectSVGNodes(dwg, tree_layout.x, tree_layout.y, c.x, c.y)
        print 'Connecting {}, {} to {}, {}'.format(tree_layout.x, tree_layout.y, c.x, c.y)
        drawEdges(c, dwg)

def drawTree(tree_layout, filepath):
    """ This functions takes a tree layout as an input, finds its
    bounds (the farther nodes horizontally and vertically), defines
    a shiny new SVG drawing, and draws the tree.
    """
    dwg = svgwrite.Drawing(filename=filepath)
    
    drawn_nodes = set()
    drawNodes(tree_layout, dwg)
    drawEdges(tree_layout, dwg)

    dwg.save()



#
# Main drawing function
#

def draw_fta(fta):
    lay = TreeLayout(fta)
    layout1(lay)
    

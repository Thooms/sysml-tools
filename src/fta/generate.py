from diagrams.internal_block import InternalBlockDiagram
from errors import DiagramError, FTAError

from collections import defaultdict, deque

class Node:
  def __init__(self):
    self.children = set()

class FTANode(Node):
  def __init__(self, name):
    Node.__init__(self)
    self.name = name

  def __str__(self):
    return self.name + ' (' + ', '.join(map(str, list(self.children))) + ')'

  def __repr__(self):
    return self.name

  def to_dict(self):
    return { "type": "FTANode", "name": self.name, "children": [c.to_dict() for c in self.children] }

class LogicalGate(Node):
  types = (
    'AND',
    'OR'
  )

  def __init__(self, gate_type):
    Node.__init__(self)
    if gate_type not in LogicalGate.types:
      raise FTAError('Unknown gate type \t' + gate_type + '.')
    else:
      self.type = gate_type

  def __str__(self):
    return self.type + ' (' + ', '.join(map(str, list(self.children))) + ')'

  def __repr__(self):
    return self.type

  def to_dict(self):
    return { "type": "LogicalGate", "gate_type": self.type, "children": [c.to_dict() for c in self.children] }

class Leaf(Node):
  types = (
    'Internal',
    'External'
  )

  def __init__(self, name, leaf_type):
    Node.__init__(self)
    self.name = name

    if leaf_type not in Leaf.types:
      raise FTAError('Unknown leaf type \t' + leaf_type + '.')
    else:
      self.type = leaf_type

  def __str__(self):
    return self.type + ' ' + self.name

  def __repr__(self):
    return self.type + ' failure of ' + self.name

  def to_dict(self):
    return { "type": "Leaf", "leaf_type": self.type, "name": self.name }

class FTA:
  def __init__(self, diagram, ports):

    if not isinstance(diagram, InternalBlockDiagram):
      raise DiagramError('Expected internal block diagram to generate fault tree')

    self.trees = []
    for _, vertex in diagram.graph.iteritems():

      # A fault tree root is a port with no successor (output port)
      # and at least one ancestor (that connects to the last Class of the IBD)

      if len(vertex.succ) == 0 and len(vertex.anc) > 0 and vertex.node_type == 'Port' and ports.has_key(vertex.node_id):
        self.trees.append(build_tree(vertex))

def build_tree(root):
  """
    Builds a fault tree from the 'root'.

    To find redundant nodes, we retrieve all OR nodes (the only nodes where an
    AND gate can be created, by construction).
    We must then collect all nodes having the same redundant stereotype (we
    put them in a map[stereotype]list_of_nodes).
  """

  root = build_node(root, set())

  q = deque([root])
  or_nodes = []

  while q:
    node = q.popleft()

    if isinstance(node, LogicalGate) and node.type == 'OR':
      or_nodes.append(node)

    for child in node.children:
      q.append(child)

  # JUST. DO. NOT. LOOK. AT. THIS.
  for node in or_nodes:
    redundant = defaultdict(list)
    for child in node.children:
      if isinstance(child, FTANode):
        for child_ in child.children:
          if isinstance(child_, LogicalGate):
            for r in child_.redundant:
              redundant[r].append(child)
    if len(redundant.keys()) > 0:
      values = redundant.values()
      node.children = set(filter(lambda x: all(x not in v for v in values), list(node.children)))
      for v in values:
        if len(v) >= 2:
          gate = LogicalGate('AND')
          gate.children = v
          node.children.add(gate)
        else:
          node.children.add(v)

  return root

def build_node(root, visit):
  """
    See the article 'Automatic Fault Tree Generation From SysML System Models'
    for more information on how to build the fault tree.
    Here we perform a simple DFS on the graph of the IBD.

      root: The root of the tree (tree defined recursively, so the root may not
        be the final root of the tree)
      visit: The set of all visited vertices in the graph of the IBD
        (output is a tree -> no cycle)
  """

  if root.node_type == 'Port':
    if len(root.anc) > 0 and len(root.anc[0].anc) > 0: # Not an input port
      node = FTANode(root.name)
      node.children = set([build_node(root.anc[0].anc[0], visit)]) # A port has only one ancestor
      return node

    else: # An input port
      return Leaf(root.name, 'External')

  elif root.node_type == 'Class':
    node = LogicalGate('OR')
    node.children.add(Leaf(root.name, 'Internal'))
    node.redundant = root.redundant

    if root.node_id not in visit:
      visit.add(root.node_id)

      for anc in root.anc:
          node.children.add(build_node(anc, visit))

      visit.remove(root.node_id)

    return node

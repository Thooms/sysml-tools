from BeautifulSoup import *
from collections import deque
from errors import FTAPSAError

from generate import FTA, LogicalGate, Leaf, FTANode


def create_leaf(soup, node):
	if isinstance(node, FTANode):
		return Tag(soup, 'gate', [('name', repr(node))])
	elif isinstance(node, Leaf):
		return Tag(soup, 'basic-event', [('name', repr(node))])
	else:
		raise FTAPSAError

def tree_to_psa(root):
	"""
		Outputs a fault tree under the OpenPSA format.

		The algorithms performs a BFS on the tree.
	"""

	soup = BeautifulSoup(
		'<?xml version="1.0" ?>\n<!DOCTYPE open-psa>\n',
		selfClosingTags=['gate', 'basic-event'])

	soup.append(Tag(soup, 'open-psa'))

	opsa = soup.find('open-psa')
	opsa.append(Tag(soup, 'define-fault-tree', [('name', 'FTA')]))

	fault_tree = opsa.find('define-fault-tree')

	q = deque([root])

	while q:
		node = q.popleft()

		if fault_tree.find('define-gate', {'name': repr(node)}) is None:
			gate = Tag(soup, 'define-gate', [('name', repr(node))])

			if len(node.children) > 0:
				tag = Tag(soup, node.children[0].type.lower(), [])

				for child in node.children[0].children:
					if isinstance(child, FTANode):
						q.append(child)
					tag.append(create_leaf(soup, child))

				gate.append(tag)

			fault_tree.append(gate)

	return soup.prettify()


def fta_to_psa(fta):
	"""
		Outputs all fault trees under the OpenPSA format.
	"""

	return [tree_to_psa(tree) for tree in fta.trees if len(tree.children) > 0]

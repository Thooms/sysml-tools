import diagrams
from errors import *

  
class InternalBlockDiagram(diagrams.Diagram):
  """
    Contains an internal block diagram.
  """
  
  def __init__(self, title):
    diagrams.Diagram.__init__(self)
    self.title = title

class InternalBlockNode(diagrams.Node):
  """
    Contains an activity node.
  """

  types = (
    'Class',
    'Port'
  )
  
  def __init__(self, node_id, name, node_type):
    diagrams.Node.__init__(self)
    self.node_id = node_id
    self.name = name
    self.parent_class = None
    self.redundant = []

    if node_type not in InternalBlockNode.types:
      raise DiagramError('Unknown node type \t' + node_type + '.')
    else:
      self.node_type = node_type
  
  def __repr__(self):
    return self.name

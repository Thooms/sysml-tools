import diagrams
from errors import *

  
class ActivityDiagram(diagrams.Diagram):
  """
    Contains an activity diagram.
  """
  
  def __init__(self, title):
    diagrams.Diagram.__init__(self)
    self.title = title

class ActivityNode(diagrams.Node):
  """
    Contains an activity node.
  """

  types = (
    # Action nodes
    'OpaqueAction',
    'CallOperationAction',
    'CallBehaviorAction',
    'SendSignalAction',
    'AcceptEventAction',

    # Object nodes
    'OutputPin',
    'InputPin',
    'CentralBufferNode',
    'InputExtension',
    'ActivityParameterNode',

    # Control nodes
    'InitialNode',
    'ForkNode',
    'DecisionNode',
    'MergeNode',
    'JoinNode',
    'FlowFinalNode',
    'ActivityFinalNode'
  )
  
  def __init__(self, node_id, name, node_type, activity=''):
    diagrams.Node.__init__(self)
    self.node_id = node_id
    self.name = name

    self.activity = activity
    self.is_activity = False

    if node_type not in ActivityNode.types:
      raise DiagramError('Unknown node type \t' + node_type + '.')
    else:
      self.node_type = node_type
  
  def __repr__(self):
    return self.name

  def is_action_node(self):
    """
      Checks whether the node is an action node or not
    """
    
    return 'Action' in self.node_type

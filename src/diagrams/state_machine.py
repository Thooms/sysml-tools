import diagrams
from errors import *

  
class StateMachineDiagram(diagrams.Diagram):
  """
    Contains a state machine diagram.
  """
  
  def __init__(self, title):
    diagrams.Diagram.__init__(self)
    self.title = title
    self.initial_state = None

class StateMachineNode(diagrams.Node):
  """
    Contains a state machine diagram node.
  """

  types = (
    'Pseudostate',
    'State'
  )
  
  def __init__(self, node_id, name, node_type):
    diagrams.Node.__init__(self)
    self.node_id = node_id
    self.name = name
    self.succ, self.anc = {}, {}

    if node_type not in StateMachineNode.types:
      raise DiagramError('Unknown node type \t' + node_type + '.')
    else:
      self.node_type = node_type
  
  def __repr__(self):
    return self.name

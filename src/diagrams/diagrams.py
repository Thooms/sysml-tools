"""
  diagrams.py

  This modules defines a generic notion of a diagram, a
  node and an edge.
  They shall be instanciated for each type of diagram, and
  have the correct exceptions thrown in an error case.
"""

class Diagram:
  """
    Generic class for a SysML diagram.
  """

  def __init__(self):
    #: Title of the diagram.
    self.title = ''

    #: Contains the graph inside the diagram, i.e. the initial node.
    self.graph = {}

class Node:
  """
    Generic class for a node.
  """

  def __init__(self):
    #: Node ID.
    self.node_id = ''

    #: Node name.
    self.name = ''

    #: Node type.
    self.node_type = ''

    #: Node successors.
    self.succ = []

    #: Node ancestors.
    self.anc = []

class Edge:
  """
    Generic class for an edge in a graph (in a diagram).
  """

  def __init__(self):
    #: Edge type.
    self.edge_type = ''

    #: Edge source (beginning). Has to be a node ID.
    self.source = ''

    #: Edge target. Has to be a node ID.
    self.target = ''

import parser.xmi
import parser.state_machine
import parser.activity
import parser.internal_block
import fmea.functional
from fta.fta_drawing import *
from fta.generate import FTA
from fta.open_psa import *
from fmea import fmea_unit, component
import nusmv.generate

import json

if __name__ == '__main__':
    
    """
    path = 'XMI/TrainDetectionSystem.xmi'
    root = parser.xmi.parse(path)
    output = nusmv.generate.from_tree(root)

    for o in output:
        print o.generate()

    """

    path = 'XMI/EMAReduced.xmi'  # 'XMI/RobosPR_With_AllocatedTo.xmi'

    
    root = parser.xmi.parse(path)

    for name, root_, applications in parser.xmi.packages_available(root):
        if 'Functional FMEA' in applications:
            print 'Functional FMEA ' + name
            diagram = parser.activity.diagram(root_)
            fmea.functional.from_diagram(diagram, 'fmea.xls')

        if 'FTA' in applications:
            print 'FTA ' + name
            diagram = parser.internal_block.diagram(root)
            fta = FTA(diagram, parser.internal_block.ports(root_))

            #print fta.trees

            for tree in fta.trees:
                #lay = TreeLayout(tree)
                lay = layout2(tree)
                drawTree(lay, 'test.svg')

    # diagram = parser.internal_block.diagram(root)
    
    # fmea_unit.collect(diagram.graph)

    #component.from_diagram(diagram, 'out.xls')

    # fta = FTA(diagram)


    # print json.dumps(fta.trees[len(fta.trees)-1].to_dict())


    # print fta.trees


    # 

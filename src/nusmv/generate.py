from diagrams.internal_block import *
from parser import state_machine

import re


def clean(s):
    return s.replace(' ', '_')


class NuSMV:
    def __init__(self, ibd, smd, failure_func):
        self.ibd = ibd
        self.smd = smd

        # Build everything that is needed to compute a NuSMV code
        self.name = clean(self.ibd.title)
        self.inputs = set([clean(v.name.lower()) for v in self.ibd.root.anc])
        self.outputs = set([clean(v.name.lower()) for v in self.ibd.root.succ])
        self.states = [clean(s.name.lower()) for s in self.smd.graph.values() if s.node_type == 'State']
        self.init_state = self.smd.initial_state.name.lower()
        self.success_states, self.failure_states = [], []
        self.build_states(failure_func)

        # Make transitions under the format transition_x
        self.transitions, i = {}, 0
        for state in self.smd.graph.values():
            self.transitions[clean(state.name.lower())] = []
            for transition, dest_state in state.succ.items():
                self.transitions[state.name.lower()].append((clean(transition.lower()) + '_' + str(i), clean(dest_state.name.lower())))
                i += 1

        self.all_transitions = flatten([map(lambda t: t[0], s) for s in self.transitions.values()])
        self.all_transitions.sort(key=lambda x: int(x[-1]))

        # print self.name
        # print self.inputs
        # print self.outputs
        # print self.all_transitions
        # print self.states
        # print self.init_state
        # print self.success_states
        # print self.failure_states

    def build_states(self, failure_func):
        for state in self.states:
            if failure_func(state):
                self.failure_states.append(state)
            else:
                self.success_states.append(state)

    def generate(self):
        state_var = 'state'

        lines = []

        lines.append('MODULE  ' + self.name + '(' + ', '.join(self.inputs) + ')')
        lines.append('VAR')

        for output in self.outputs:
            lines.append('\t' + output + ' : boolean;')

        lines.append('')

        for transition in self.all_transitions:
            lines.append('\t' + transition + ' : boolean;')

        lines.append('')

        lines.append('\t' + state_var + ' : {' + ', '.join(self.states) + '};')

        lines.append('')

        lines.append('ASSIGN')
        lines.append('init(' + state_var + ') := ' + self.init_state + ';')

        lines.append('next(' + state_var + ') := ')
        lines.append('\tcase')

        for state in self.success_states:
            for transition, dest_state in self.transitions[state]:
                s = '\t\t' + state_var + ' = ' + state + ' & '
                s += transition + ' = TRUE : ' + dest_state + ';'
                lines.append(s)

        lines.append('esac;')
        lines.append('ASSIGN')

        for output in self.outputs:
            sep = ''
            if len(self.inputs) > 0 and len(self.failure_states) > 0:
                sep = ' & '
            lines.append('\t' + output + ' := ' + ' & '.join(self.inputs) + sep + ' & '.join(['!' + x for x in self.failure_states]) + ';')
        

        return '\n'.join(lines)

    def __repr__(self):
        return self.ibd.title + ' - ' + self.smd.title


def failure_func(state):
    return state.startswith('fail')


def flatten(x):
    if isinstance(x, list):
        return [a for i in x for a in flatten(i)]
    else:
        return [x]


def find(obj, expr, *args, **kwargs):
    """
        Finds an expression within an object, case insentively
    """

    if isinstance(expr, list):
        return flatten([find(obj, e, *args, **kwargs) for e in expr])

    return obj.find(re.compile(expr, re.IGNORECASE), *args, **kwargs)


def find_all(obj, expr, *args, **kwargs):
    """
        Finds all occurences of an expression within an object,
        case insentively
    """

    if isinstance(expr, list):
        return flatten([find_all(obj, e, *args, **kwargs) for e in expr])

    return obj.findAll(re.compile(expr, re.IGNORECASE), *args, **kwargs)


def build_ibd(ibd_root, root, diagram):
    root_id = ibd_root.get('xmi:id')
    root_name = diagram.title
    root_type = 'Class'

    diagram.graph[root_id] = InternalBlockNode(root_id, root_name, root_type)
    diagram.root = diagram.graph[root_id]

    for port in find_all(ibd_root, 'ownedattribute',
                        {'xmi:type': 'uml:Port'}):
        node_id = port.get('xmi:id')
        node_name = port.get('name')
        node_type = 'Port'

        diagram.graph[node_id] = InternalBlockNode(node_id, node_name,
                                                   node_type)

    for connector in find_all(root, 'ownedconnector',
                              {'xmi:type': 'uml:Connector'}):
        source, target = map(lambda x: x.get('role'),
                             find_all(connector, 'end', limit=2))

        if source in diagram.graph:
            diagram.root.succ.append(diagram.graph[source])
            diagram.graph[source].anc.append(diagram.root)
        elif target in diagram.graph:
            diagram.root.anc.append(diagram.graph[target])
            diagram.graph[target].succ.append(diagram.root)

def from_tree(root):
    output = []

    for cl in find_all(root, 'packagedelement', {'xmi:type': 'uml:Class'}):
        ibd = InternalBlockDiagram(cl.get('name'))
        smd_root = find(cl, 'ownedbehavior', {'xmi:type': 'uml:StateMachine'})

        if smd_root:
            build_ibd(cl, root, ibd)
            smd = state_machine.diagram(smd_root)
            output.append(NuSMV(ibd, smd, failure_func))

    return output

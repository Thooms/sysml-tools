from functools import partial

# Modules used to build the GUI
import Tkinter
import tkFileDialog
import tkMessageBox

import parser.xmi

class GUIButton:
  """
    Class representing a button in the GUI gridview

      text: The text shown in the button
      row: row of the button in the grid
      column: column of the button in the grid
      command: command executed as soon as the button has been pressed
      needs_xmi: if True, means that 'command' needs the XMI tree to work
  """

  def __init__(self, text, row, column, command, needs_xmi):
    self.text = text
    self.row, self.column = row, column
    self.command = command
    self.needs_xmi = needs_xmi

class ListContent:
  def __init__(self, name, root, applications):
    self.name = name
    self.root = root
    self.applications = applications

  def __str__(self):
    return self.name

class SysMLMenu(Tkinter.Frame):
  """
    Main menu class, with GUI window

      master: 
      on_input_buttons: iterable (list, tuple, generator...) of GUIButton
        representing all the buttons that are present on the GUI gridview
  """

  def __init__(self, master, on_input_buttons):
    Tkinter.Frame.__init__(self, master)
    self.master = master
    self.build()
    self.input_xmi = None # The XMI file that will be loaded
    self.on_input_buttons = on_input_buttons
    self.contents = []
  
  def build(self):
    """
      Method to build the grid at launch, with only one button: 'Open XMI'
    """

    self.master.title('SysML Tools') # Window title
    self.pack(fill=Tkinter.BOTH, expand=1)
    
    # Main button: open an XMI file. on_open will be executed as soon as the button is pressed
    open_file = Tkinter.Button(self, text='Open XMI', command=self.on_open)
    open_file.grid(row=0, column=0)
    
  def on_open(self):
    """
      Command executed as soon as the 'Open XMI' button is pressed
    """

    # Files listed, '.xmi' by default, but the user can choose to list all files too
    file_types = [('XMI Files', '*.xmi'), ('All Files', '*')]

    dialog = tkFileDialog.Open(self, filetypes=file_types) # Open a file explorer to let the user choose the XMI file
    self.input_xmi = dialog.show() # dialog.show() will return the path to the chosen XMI file 
    
    if self.input_xmi:

      # Parse xmi on load -> parsed once
      self.root = parser.xmi.parse(self.input_xmi)

      # Normalize the tree (see method doctype for more informations)
      # arser.xmi.normalize(self.root)

      list_box = Tkinter.Listbox(self)
      self.contents = []

      for package, root_, applications in parser.xmi.packages_available(self.root):
        self.contents.append(ListContent(package, root_, applications))
        list_box.insert(Tkinter.END, self.contents[-1])

      list_box.grid(row=1, column=0)

      for button in self.on_input_buttons:
        # If the buttons needs the XMI tree, precompute the command with the tree as parameter
        if button.needs_xmi:
          command = partial(button.command, root=self.root, listbox=list_box, contents=self.contents)
        else:
          command = partial(button.command, listbox=list_box, contents=self.contents)

        # Puts the button on the grid
        new_button = Tkinter.Button(self, text=button.text, command=command)
        new_button.grid(row=button.row, column=button.column)

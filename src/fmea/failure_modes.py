# The list of all failure modes available in a functional fmea

failure_modes = [
  "Fails to perform",
  "Performs incorrectly (degraded performance)",
  "Operates inadvertently",
  "Operates at incorrect time (early, late)",
  "Unable to stop operation",
  "Receives erroneous data",
  "Sends erroneous data"
]
"""
fmea.fmea_unit.py

FMEA unit, representing a function in the functional FMEA
"""

from collections import defaultdict

import failure_modes
from diagrams.activity import ActivityDiagram

class FMEAUnit:
  """
    Contains a left cell for the to-be-generated functional FMEA.
  """
  
  def __init__(self, name, input_param, output_param, upstream, downstream):
    self.name = name
    self.failure_modes = list(failure_modes.failure_modes)
    self.input_param = input_param
    self.output_param = output_param
    self.upstream = upstream
    self.downstream = downstream

  def __repr__(self):
    return self.name

def local_dfs(node, acc, end_function, ascend, visit):
  if node.node_id not in visit:
    visit.add(node.node_id)
    children = node.anc if ascend else node.succ
    for child in children:
      if end_function(child):
        acc.add(child.name)
      else:
        local_dfs(child, acc, end_function, ascend, visit)

def find_input_pins(node):
  return [x for x in node.anc if x.node_type == 'InputPin']

def find_output_pins(node):
  return [x for x in node.succ if x.node_type == 'OutputPin']

def find_stream(node, ascend):
  output = set()
  local_dfs(
    node=node,
    acc=output,
    end_function=lambda x: x.is_action_node() and x.name != node.name,
    ascend=ascend,
    visit=set()
  )
  return output

def add_to_output(node, output):
  # if node.is_activity or node.node_type == 'AcceptEventAction':
  #   return
  
  unit = FMEAUnit(
    name=node.name,
    input_param=find_input_pins(node),
    output_param=find_output_pins(node),
    upstream=find_stream(node, ascend=True),
    downstream=find_stream(node, ascend=False)
  )

  output[node.activity].append(unit)

def dfs(node, output, visit):
  if node.node_id not in visit:
    visit[node.node_id] = True
    if node.is_action_node():
      add_to_output(node, output)
    for child in node.succ:
      dfs(child, output, visit)

def collect(graph, component=False):
  output = defaultdict(list)

  for node in graph.values():
    if node.is_activity:
      input_nodes = find_input_pins(node)
      input_param, upstream = set(), set()

      for param in input_nodes:
        input_param.add(param.name)

        if len(param.activity) > 0:
          upstream.add(param.activity)

      output_nodes = find_output_pins(node)
      output_param, downstream = set(), set()

      for param in output_nodes:
        output_param.add(param.name)

        if len(param.activity) > 0:
          downstream.add(param.activity)

      unit = FMEAUnit(
        name=node.name,
        input_param=list(input_param),
        output_param=list(output_param),
        upstream=list(upstream),
        downstream=list(downstream)
      )

      if component:
        output[node.class_].append(unit)
      else:
        output[node.name].append(unit)

  return output

def failureModesCount():
  return len(failure_modes.failure_modes)

"""
fmea.functional.py

Functional FMEA generation, export to XLS format
"""

from xlwt import *

from diagrams.activity import ActivityDiagram
from errors import DiagramError
import fmea_unit

LIST_SEPARATOR = '\n'

# List of all colors available for cells
colors = [0x31, 0x36, 0x3C, 0x1D, 0x0F, 0x12, 0x3A, 0x11,
0x1C, 0x10, 0x38, 0x13, 0x33, 0x17, 0x16, 0x37, 0x17, 0x3F, 0x11, 0x1F, 0x3E,
0x1A, 0x2E, 0x30, 0x2A, 0x34, 0x29, 0x2B, 0x32, 0x0E, 0x1E, 0x13, 0x3B, 0x35,
0x2C, 0x18, 0x0E, 0x3D, 0x14, 0x0A, 0x2D, 0x39, 0x16, 0x28, 0x2F, 0x15, 0x15,
0x0F, 0x14, 0x09, 0x0D]

def buildUnitsDict(units):
  res = {}
  for unit in units:
    causal_factors, imm_effects = '', ''
    if len(unit.input_param) > 0:
      causal_factors += 'input parameters:\n' + LIST_SEPARATOR.join(unit.input_param)
    if len(unit.output_param) > 0:
      if len(causal_factors) > 0:
        causal_factors += '\n'
      causal_factors += 'output parameters:\n' + LIST_SEPARATOR.join(unit.output_param)

    if len(unit.upstream) > 0:
      imm_effects += 'upstream:\n' + LIST_SEPARATOR.join(unit.upstream) + '\n'
    if len(unit.downstream) > 0:
      if len(imm_effects) > 0:
        imm_effects += '\n'
      imm_effects += 'downstream:\n' + LIST_SEPARATOR.join(unit.downstream)

    res[unit.name] = [[mode, causal_factors, imm_effects, "", "", ""] for mode in unit.failure_modes]
  return res

def getColumns(header, d):
  columns = [[h] for h in header]
  for name, content in d.iteritems():
    columns[0].append(name)
    for fm in content:
      for k, v in enumerate(fm):
        columns[k+1].append(v)
  return columns

def cellLen(cell):
  max_, curr = 0, 0
  for c in cell:
    if c == '\n':
      max_ = max(max_, curr)
      curr = 0
    else:
      curr += 1
  max_ = max(max_, curr)
  return max_

def writeListRow(row, elements, start=0, style=easyxf('')):
  for i, element in enumerate(elements):
    row.write(start+i, element, style)
    row.height = max(row.height, (1 + element.count('\n')) * 256)   

def export(name, activities, filename):
  w = Workbook()
  sheet = w.add_sheet(name)
  header = [
    "Component",
    "Function",
    "Function failure mode",
    "Causal factors",
    "Immediate Effects",
    "System Effects",
    "Recommended actions",
    "Severity"
  ]

  writeListRow(sheet.row(0), header, style=easyxf('font : bold on; align : horizontal center; \
    borders : top thin, bottom thin, left thin, right thin;'))

  line, overall_dict = 1, {}
  for color, (class_, units) in enumerate(activities.iteritems()):
    d = buildUnitsDict(units)
    overall_dict.update(d)
    style = easyxf('align: vertical center, horizontal center; \
      pattern: pattern solid; \
      borders : top thin, bottom thin, left thin, right thin;')
    style.pattern.pattern_fore_colour = colors[color]

    sheet.write_merge(line, line + (len(units) - 1) * (fmea_unit.failureModesCount() - 1), 0, 0, class_, style)

    for name, content in d.iteritems():
      sheet.write_merge(line, line+len(content)-1, 1, 1, name, style)

      for i, failure_mode in enumerate(content):
        writeListRow(sheet.row(line+i), failure_mode, start=2, style=style)
      line += len(content)

  # Adapt columns width (width of 1 char ~ 256) 
  for i, column in enumerate(getColumns(header, overall_dict)):
    sheet.col(i).width = max(map(cellLen, column)) * 256 + 5 * 256
  
  w.save(filename)

def from_diagram(diagram, filename):
  if not isinstance(diagram, ActivityDiagram):
    raise DiagramError('Expected activity diagram to generate an FMEA')

  units = fmea_unit.collect(diagram.graph, component=True)
  export(diagram.title, units, filename)

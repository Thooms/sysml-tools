from diagrams.state_machine import *

import re


def flatten(x):
    if isinstance(x, list):
        return [a for i in x for a in flatten(i)]
    else:
        return [x]


def find(obj, expr, *args, **kwargs):
    """
        Finds an expression within an object, case insentively
    """

    if isinstance(expr, list):
        return flatten([find(obj, e, *args, **kwargs) for e in expr])

    return obj.find(re.compile(expr, re.IGNORECASE), *args, **kwargs)


def find_all(obj, expr, *args, **kwargs):
    """
        Finds all occurences of an expression within an object, case insentively
    """

    if isinstance(expr, list):
        return flatten([find_all(obj, e, *args, **kwargs) for e in expr])

    return obj.findAll(re.compile(expr, re.IGNORECASE), *args, **kwargs)


def build_graph(diagram, root):
    for vertex in find_all(root, 'subvertex'):
        node_id = vertex.get('xmi:id')
        node_type = vertex.get('xmi:type')[4:]
        node_name = vertex.get('name')

        diagram.graph[node_id] = StateMachineNode(node_id, node_name, node_type)
 
    for transition in find_all(root, 'transition', { 'xmi:type': 'uml:Transition' }):
        source, target = transition.get('source'), transition.get('target')
        effect = find(transition, 'effect')
        name = ''
        if effect:
            name = effect.get('body')

        src, tgt = diagram.graph[source], diagram.graph[target]

        if src.node_type == 'Pseudostate' and src.name == 'Initial':
            diagram.initial_state = tgt
        else:
            src.succ[name] = tgt
            tgt.anc[name] = src


def diagram(root):
    """
        Generates diagram from XMI tree.
    """

    diagram = StateMachineDiagram(root.get('name'))
    build_graph(diagram, root)

    return diagram

"""
activity_parser.py

Implements the activity diagrams XMI files parsing.
"""

from itertools import chain
from diagrams.activity import *

import re


def flatten(x):
    if isinstance(x, list):
        return [a for i in x for a in flatten(i)]
    else:
        return [x]

def find(obj, expr, *args, **kwargs):
  """
    Finds an expression within an object, case insentively
  """

  if isinstance(expr, list):
    return flatten([find(obj, e, *args, **kwargs) for e in expr])
  
  return obj.find(re.compile(expr, re.IGNORECASE), *args, **kwargs)

def find_all(obj, expr, *args, **kwargs):
  """
    Finds all occurences of an expression within an object, case insentively
  """
  
  if isinstance(expr, list):
    return flatten([find_all(obj, e, *args, **kwargs) for e in expr])

  return obj.findAll(re.compile(expr, re.IGNORECASE), *args, **kwargs)


def get_activities(root):
  """
    Collects activities in a xmi tree.
    Also removes prefixes such as '{http://schema.omg.org/spec/XMI/2.1}'
    in tags and attributes.
  """

  activities = {}

  for element in find_all(root, ['packagedElement', 'ownedBehavior']):
    for key, attr in element.attrs:
      if key == 'xmi:type' and attr == 'uml:Activity':
        activities[element.get('xmi:id')] = element

  return activities

def get_classes(root):
  classes = {}
  for element in root.findAll(['packagedelement', 'ownedbehavior']):
    for key, attr in element.attrs:
      if key == 'xmi:type' and attr == 'uml:Class':
        classes[element.get('xmi:id')] = element
  return classes

def input_output_pins(diagram, element, graph_node, root):
  """
    Collects the input and output pins from a node and
    adds them to the graph.
  """

  to_chain = filter(lambda x: x is not None,
    [find_all(element, 'argument', recursive=True),
     find_all(element, 'result', recursive=True)])

  for pin in chain(*to_chain):
    pin_type = pin.get('xmi:type')[4:]
    pin_id = pin.get('xmi:id')
    pin_name = pin.get('name')

    if pin_type in ('InputPin', 'OutputPin'):
      new_pin = ActivityNode(pin_id, pin_name, pin_type)
      diagram.graph[pin_id] = new_pin

      if pin_type == 'InputPin':
        new_pin.succ.append(graph_node)
        graph_node.anc.append(new_pin)

        incoming = pin.get('incoming')
        if incoming is not None:
          outgoing = find(root, 'result', {'outgoing': incoming})
          if outgoing is not None:
            new_pin.activity = outgoing.parent.get('name')

      else:
        new_pin.anc.append(graph_node)
        graph_node.succ.append(new_pin)

        outgoing = pin.get('outgoing')
        if outgoing is not None:
          incoming = find(root, 'argument', {'incoming': outgoing})
          if incoming is not None:
            new_pin.activity = incoming.parent.get('name')

def add_to_graph(diagram, element, root):
  """
    Adds a new node to the graph.
    A node is made up of its id, name, type and
    the activity it belongs to.
    
    If the node is an action node, input and output
    pins are also collected.
  """

  node_type = element.get('xmi:type')[4:] # Remove the 'uml:' prefix
  node_id = element.get('xmi:id')
  node_name = element.get('name')

  node = ActivityNode(node_id, node_name, node_type)
  diagram.graph[node_id] = node

  if node.is_action_node():
    input_output_pins(diagram, element, node, root)

  return node

def get_class(root, classes, activity_id):
  for allocated in find_all(root, 'sysml:Allocated'):
    id_allocated, id_activity = allocated.get('xmi:id')[:37], allocated.get('xmi:id')[37:]

    if id_activity == activity_id:
      for base_allocated in find_all(root, 'sysml:Allocated'):
        base_element = base_allocated.get('base_NamedElement')
        if base_element is not None:
          id_ = base_element.get('xmi:id')[:37]

          if id_ == id_allocated:
            return classes.get(base_element)

def build_graph(diagram, activities, root):
  classes = get_classes(root)

  for node in find_all(root, 'node'):
    activity_node = add_to_graph(diagram, node, root)

    # If the node is also an uml:Activity -> Compute its real name
    behavior = node.get('behavior')
    if activity_node.node_type == 'CallBehaviorAction' and behavior is not None:
      activity_node.is_activity = True
      activity_node.name = activities[behavior].get('name')
      activity_node.class_ = get_class(root, classes, behavior)

  for edge in find_all(root, 'edge'):
    edge_source, edge_target = edge.get('source'), edge.get('target')
    source, target = diagram.graph[edge_source], diagram.graph[edge_target]
    source.succ.append(target)
    target.anc.append(source)

def diagram(root):
  """
    Generates diagram from XMI tree.
  """
  
  activities = get_activities(root)  
  diagram = ActivityDiagram(root.get('name'))
  build_graph(diagram, activities, root)

  return diagram

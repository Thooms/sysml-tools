"""
xmi.py

Parses XMI files.
"""

import re

from BeautifulSoup import BeautifulSoup
# from errors import BadXMIException

def flatten(x):
    if isinstance(x, list):
        return [a for i in x for a in flatten(i)]
    else:
        return [x]

def find(obj, expr, *args, **kwargs):
  """
    Finds an expression within an object, case insentively
  """

  if isinstance(expr, list):
    return flatten([find(obj, e, *args, **kwargs) for e in expr])
  
  return obj.find(re.compile(expr, re.IGNORECASE), *args, **kwargs)

def find_all(obj, expr, *args, **kwargs):
  """
    Finds all occurences of an expression within an object, case insentively
  """
  
  if isinstance(expr, list):
    return flatten([find_all(obj, e, *args, **kwargs) for e in expr])

  return obj.findAll(re.compile(expr, re.IGNORECASE), *args, **kwargs)

def normalize(root):
  pass

def parse(filepath):
  with open(filepath, 'r') as f:
    s = f.read()

  BeautifulSoup.NESTABLE_TAGS['packagedelement'] = ['packagedelement']

  return BeautifulSoup(s)

def packages_available(root):
  for package in find_all(root, 'packagedelement', {'xmi:type': 'uml:Package'}):
    class_ = find(package, 'packagedelement', {'xmi:type': 'uml:Class'}, recursive=False)
    connector = find(package, 'ownedconnector', {'xmi:type': 'uml:Connector'})
    activity = find(package, 'packagedelement', {'xmi:type': 'uml:Activity'}, recursive=False)
    node = find(package, 'node', {'xmi:type':'uml:CallBehaviorAction'})

    out = []

    if class_ is not None and connector is not None:
      out.append('FTA')
      out.append('Component FMEA')

    if activity is not None and node is not None:
      out.append('Functional FMEA')

    if len(out):
      yield (package.get('name'), package, out)

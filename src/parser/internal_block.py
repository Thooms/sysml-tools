
"""
internal_block_parser.py

Implements the internal block diagrams XMI files parsing.
"""

from diagrams.internal_block import *

import re


def flatten(x):
    if isinstance(x, list):
        return [a for i in x for a in flatten(i)]
    else:
        return [x]

def find(obj, expr, *args, **kwargs):
  """
    Finds an expression within an object, case insentively
  """

  if isinstance(expr, list):
    return flatten([find(obj, e, *args, **kwargs) for e in expr])
  
  return obj.find(re.compile(expr, re.IGNORECASE), *args, **kwargs)

def find_all(obj, expr, *args, **kwargs):
  """
    Finds all occurences of an expression within an object, case insentively
  """
  
  if isinstance(expr, list):
    return flatten([find_all(obj, e, *args, **kwargs) for e in expr])

  return obj.findAll(re.compile(expr, re.IGNORECASE), *args, **kwargs)

def get_classes(root):
  classes = {}
  for element in root.findAll(['packagedelement', 'ownedbehavior']):
    for key, attr in element.attrs:
      if key == 'xmi:type' and attr == 'uml:Class':
        classes[element.get('xmi:id')] = element
  return classes

def add_to_graph(graph, element):
  """
    Adds a new node (from the XMI element 'element') to the existing graph
  """

  node_id = element.get('xmi:id')
  node_type = element.get('xmi:type')[4:] # Remove the 'xmi:' prefix
  node_name = element.get('name')

  node = InternalBlockNode(node_id, node_name, node_type)
  graph[node_id] = node

  return node

def build_graph(diagram, classes, root):
  connectors = []
  main_class = None

  # Collects vertices for the graph
  for class_id, class_ in classes.iteritems():
    for connector in find_all(class_, 'ownedConnector', recursive=False):
      main_class = class_ # Main class is the only one with connectors
      connectors.append(connector)

    if class_ != main_class:
      node = add_to_graph(diagram.graph, class_)

    for attribute in find_all(class_, 'ownedAttribute', recursive=False):
      if attribute.get('xmi:type') == 'uml:Port':
        port = add_to_graph(diagram.graph, attribute)

        if class_ != main_class:
          port.parent_class = node # Save the class the port belongs to

  # Collects edges for the graph
  for connector in connectors:
    source_element, target_element = find_all(connector, 'end', limit=2)
    source_id, target_id = source_element.get('role'), target_element.get('role')

    if source_id in diagram.graph and target_id in diagram.graph:
      source_node, target_node = diagram.graph[source_id], diagram.graph[target_id]

      source_node.succ.append(target_node)
      target_node.anc.append(source_node)

      if source_node.parent_class is not None:
        source_node.anc.append(source_node.parent_class)
        source_node.parent_class.succ.append(source_node)

      if target_node.parent_class is not None:
        target_node.succ.append(target_node.parent_class)
        target_node.parent_class.anc.append(target_node)

  for redundant in find_all(root, 'SafetyProfile:Redundant', recursive=True):
    id_ = redundant.get('xmi:id')
    safety_profile, node_id = id_[:37], id_[37:]  # 37 : the size of a XMI ID

    diagram.graph[node_id].redundant.append(safety_profile)


def ports(root):
  res = {}
  for port in find_all(root, 'ownedAttribute', {'xmi:type': 'uml:Port'}):
    res[port.get('xmi:id')] = True
  return res


def diagram(root):
  """
    Generates diagram from XMI tree.
  """
  
  classes = get_classes(root)
  diagram = InternalBlockDiagram(root.get('name'))
  build_graph(diagram, classes, root)

  return diagram

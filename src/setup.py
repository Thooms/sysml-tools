from distutils.core import setup
import py2exe

setup(
  console = [
    'main.py',
    'diagrams/diagrams.py', 'diagrams/activityDiagrams.py',
    'failure_modes.py', 'fmea_unit.py',
    'functional_fmea.py', 'xmi_parser.py'
  ]
)
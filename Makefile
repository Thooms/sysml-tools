
clean:
	rm -rf src/*.pyc doc/

doc:
	epydoc -v src/*.py src/diagrams --html -o doc/
